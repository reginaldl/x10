#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "x10.h"

int main(int argc, char **argv)
{
	int code;
	int pin_fd;

	if (argc < 3) {
		printf("%s <pin_number> <code>\n", argv[0]);
		return -1;
	}
	code = atoi(argv[2]);
	if (code > 8388607 || code < 0) {
		printf("Invalid code.\n");
		return -1;
	}
	pin_fd = open_pin(atoi(argv[1]), X10_OUT);
	if (pin_fd < 0) {
		printf("Could not open pin %s\n", argv[1]);
		return -1;
	}
	send_22bit_code(pin_fd, code);
	close(pin_fd);
	return 0;
}
