Basic implementation of X10 RF protocol for Beagle Board / Raspberry Pi.

```
$ gcc send.c x10.c -o send
$ gcc sniffer.c x10.c -o sniffer 
```
