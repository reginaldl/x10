#include <time.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/time.h>

#include "x10.h"

int open_pin(int pin, pin_direction_t direction)
{
	int fd;
	int len;
	ssize_t ret;
	char number[8];
	char path[1024];

	len = snprintf(number, sizeof(number), "%d\n", pin);
	if (len >= sizeof(number)) {
		return -1;
	}
	fd = open("/sys/class/gpio/export", O_WRONLY);
	if (fd < 0) {
		return -1;
	}
	ret = write(fd, number, len);
	/* if this pin has already been exported, write will fail and errno == EBUSY */
       	if (ret != len && errno != EBUSY) {
		close(fd);
		return -1;
	}
	close(fd);
	len = snprintf(path, sizeof(path), "/sys/class/gpio/gpio%d/direction", pin);
	if (len >= sizeof(path)) {
		return -1;
	}
	fd = open(path, O_WRONLY);
	if (fd < 0) {
		return -1;
	}
	switch (direction) {
		case X10_IN:
			ret = write(fd, "in\n", 3);
			break;
		case X10_OUT:
			ret = write(fd, "out\n", 4);
	}
	close(fd);
	if (ret < 0) {
		return -1;
	}
	if (direction == X10_IN) {
		/* Enable gpio edge for this direction */
		len = snprintf(path, sizeof(path), "/sys/class/gpio/gpio%d/edge", pin);
		if (len >= sizeof(path)) {
			return -1;
		}
		fd = open(path, O_WRONLY);
		if (fd < 0) {
			return -1;
		}
		if (write(fd, "both\n", 5) < 0) {
			return -1;
		}
		close(fd);
	}
	len = snprintf(path, sizeof(path), "/sys/class/gpio/gpio%d/value", pin);
	if (len >= sizeof(path)) {
		return -1;
	}
	return open(path, O_WRONLY);
}

void send_lock(int pin_fd)
{
	usleep(5000);
	write(pin_fd, "1", 1);
	usleep(100);
	write(pin_fd, "0", 1);
	usleep(500);
	write(pin_fd, "0", 1);
}

void send(int pin_fd, int bit)
{
	write(pin_fd, "1", 1);
	usleep(bit ? 500 : 80);
	write(pin_fd, "0", 1);
	usleep(bit ? 80 : 500);
}

void send_22bit_code(int pin_fd, int code)
{
	int i;
	int j;
	int mask;
	char buff[24];

	/* Send code multiple times */
	for (i = 0; i < 10; i++) {
		send_lock(pin_fd);
		memset(buff, 0, sizeof(buff));
		for (j = 22; j >= 0; j--) {
			if (code & (1 << j)) {
				buff[22 - j] = '1';
				send(pin_fd, 1);
			} else {
				buff[22 - j] = '0';
				send(pin_fd, 0);
			}
		}
		send(pin_fd, 0);
		printf("%s\n", buff);
	}
}


