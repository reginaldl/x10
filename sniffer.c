#include <stdio.h>
#include <stdlib.h>
#include <poll.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>

#include "x10.h"

#define MAX_CHANGES 47

int tick()
{
	struct timeval diff;
	struct timeval curtime;
	static struct timeval lasttime;

	gettimeofday(&curtime, 0);
	timersub(&curtime, &lasttime, &diff);
	lasttime = curtime;
	return diff.tv_usec;
}

void print_code(int *bits, int size)
{
	int i;
	int code = 0;
	if (size < 6) {
		return;
	}
	for (i = 0; i < size; i++) {
		printf("%d ", bits[i]);
	}
	printf("\n");
	for (i = 0; i < size - 1; i += 2) {
		if (bits[i] < 400 && bits[i + 1] > 400) {
			code = code << 1;
			printf("0");
		} else if (bits[i] > 400 && bits[i + 1] < 400) {
			code = code << 1;
			code++;
			printf("1");
		} else {
			printf("?");
		}
	}
	printf("\n");
	printf("code: %d\n", code);
}

int main(int argc, char **argv)
{
	int fd;
	int rc;
	int len;
	int lock;
	int counter;
	int curtick;
	int bits[1000];
	char buf[64];
	struct pollfd fdset[1];

	fd = open_pin(68, X10_IN);
	if (fd < 0) {
		printf("Could not open PIN\n");
		return -1;
	}
	lock = 0;
	counter = -1;
	memset(bits, 0, sizeof(bits));
	while (42) {
		memset(fdset, 0, sizeof(fdset));
		fdset[0].fd = fd;
		fdset[0].events = POLLPRI;
		rc = poll(fdset, 1, 1000);
		if (rc < 0) {
			printf("poll failed.\n");
			return -1;
		} else if (rc == 0) {
			print_code(bits, counter);
			counter = -1;
			memset(bits, 0, sizeof(bits));
			lock = 0;
		} else if (fdset[0].revents & POLLPRI) {
			lseek(fd, 0, SEEK_SET);
			len = read(fdset[0].fd, buf, sizeof(buf));

			curtick = tick();
			if (counter < 0) {
				counter++;
			} else {
				if (lock != 3) {
					if (curtick > 4000 && lock == 0) {
						printf("%d ", curtick);
						lock++;
					} else if (curtick < 300 && lock == 1) {
						printf("%d ", curtick);
						lock++;
					} else if (curtick > 500 && lock == 2) {
						printf("%d | ", curtick);
						lock++;
						printf("lock\n");
					} else {
						lock = 0;
					}
				} else {
					if (counter < MAX_CHANGES) {
						bits[counter++] = curtick;
					} else {
						print_code(bits, counter);
						counter = -1;
						memset(bits, 0, sizeof(bits));
						lock = 0;
					}
				}
			}
		}
	}
	close(fd);
	return 0;
}
