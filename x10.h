#ifndef X10_H_
#define X10_H_

typedef enum pin_direction_e {
	X10_IN,
	X10_OUT
} pin_direction_t;

int open_pin(int pin, pin_direction_t direction);
void send_22bit_code(int pin_fd, int code);

#endif /* X10_H_ */
